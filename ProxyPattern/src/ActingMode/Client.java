package ActingMode;

//客户
public class Client {
    public static void main(String[] args) {
        //直接找房东去租房子
        new Host().rent();

        //找中介来租房子
        new Proxy(new Host()).rent();
    }
}
