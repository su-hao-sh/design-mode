package ActingMode.Dynamicagent;
//接口
public interface UserService {
    public void add();
    public void delete();
    public void update();
    public void query();
}
