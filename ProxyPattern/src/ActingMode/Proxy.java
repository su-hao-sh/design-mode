package ActingMode;

//代理角色，这个例子中指的是中介
public class Proxy {
    private Host host;

    public Proxy(Host host) {
        this.host = host;
    }

    public void rent(){
        setHouse();
        host.rent();
        contract();
        fee();
    }

    //中介带你看房子
    public void setHouse(){
        System.out.println("中介带你看房子");
    }

    //收中介费
    public void fee(){
        System.out.println("中介收中介费");
    }

    //签合同
    public void contract(){
        System.out.println("签署租赁合同");
    }
}
