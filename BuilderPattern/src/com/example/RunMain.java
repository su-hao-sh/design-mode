package com.example;

import com.example.Product.Car;
import com.example.bulders.CarBuilder;

/**
 * @Author 苏浩
 * @Date 2022/12/3 18:41
 * @Version 1.0
 */
public class RunMain {
    public static void main(String[] args) {
        Director director = new Director();

        CarBuilder builder = new CarBuilder();
        director.constructSportsCar(builder);
        Car car = builder.getResult();
        System.out.println("Car built:\n" + car.getCarType());
    }

}
