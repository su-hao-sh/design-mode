package com.example.bulders;

import com.example.Product.Car;
import com.example.Product.CarType;
import com.example.Product.Engine;
import com.example.Product.TripComputer;

/**
 * @Author 苏浩
 * @Date 2022/12/3 18:33
 * @Version 1.0
 * 实现Builder的方法
 */
public class CarBuilder implements Builder{
    private CarType type;
    private int seats;
    private Engine engine;
    private TripComputer tripComputer;

    public void setCarType(CarType type) {
        this.type = type;
    }

    @Override
    public void setSeats(int seats) {
        this.seats = seats;
    }

    @Override
    public void setEngine(Engine engine) {
        this.engine = engine;
    }


    @Override
    public void setTripComputer(TripComputer tripComputer) {
        this.tripComputer = tripComputer;
    }


    public Car getResult() {
        return new Car(type, seats, engine, tripComputer);
    }
}
