package com.example.bulders;

import com.example.Product.CarType;
import com.example.Product.Engine;
import com.example.Product.TripComputer;

/**
 * @Author 苏浩
 * @Date 2022/12/3 18:31
 * @Version 1.0
 * @description 抽象出产品的固有方法（不经常变动的代码）
 */
public interface Builder {
    void setCarType(CarType type);
    void setSeats(int seats);
    void setEngine(Engine engine);
    void setTripComputer(TripComputer tripComputer);
}
