package com.example;

import com.example.Product.CarType;
import com.example.Product.Engine;
import com.example.Product.TripComputer;
import com.example.bulders.Builder;

/**
 * @Author 苏浩
 * @Date 2022/12/3 18:39
 * @Version 1.0
 */
public class Director{
    public void constructSportsCar(Builder builder) {
        builder.setCarType(CarType.SPORTS_CAR);
        builder.setSeats(2);
        builder.setEngine(new Engine(3.0, 0));
        builder.setTripComputer(new TripComputer());
    }

    public void constructCityCar(Builder builder) {
        builder.setCarType(CarType.CITY_CAR);
        builder.setSeats(2);
        builder.setEngine(new Engine(1.2, 0));
        builder.setTripComputer(new TripComputer());
    }

    public void constructSUV(Builder builder) {
        builder.setCarType(CarType.SUV);
        builder.setSeats(4);
        builder.setEngine(new Engine(2.5, 0));
    }
}
