package com.example.Product;

/**
 * @Author 苏浩
 * @Date 2022/12/3 18:33
 * @Version 1.0
 */
public enum CarType {
    CITY_CAR, SPORTS_CAR, SUV
}
