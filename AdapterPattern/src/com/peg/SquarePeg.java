package com.peg;

/**
 * 方形的钉子
 * @Author 苏浩
 * @Date 2022/12/2 21:02
 * @Version 1.0
 */
public class SquarePeg extends Peg {
    private double size;//方形钉的大小

    public SquarePeg() {}

    public SquarePeg(double size) {
        this.size = size;
    }

    public double getSize() {
        return size;
    }
}


