package com.peg;

/**
 * 圆形的钉子
 * @Author 苏浩
 * @Date 2022/12/2 21:02
 * @Version 1.0
 */
public class RoundPeg extends Peg {
    private double size;//圆形钉的大小

    public RoundPeg() {}

    public RoundPeg(double size) {
        this.size = size;
    }

    public double getSize() {
        return size;
    }
}


