package com.AdapterSquare;

import com.peg.RoundPeg;
import com.peg.SquarePeg;

/**
 * 方形钉子的适配器
 * @Author 苏浩
 * @Date 2022/12/2 21:04
 * @Version 1.0
 */
public class AdapterSquare extends RoundPeg {
    private SquarePeg peg;//需要方形钉子的对象

    public AdapterSquare(SquarePeg peg) {
        this.peg = peg;
    }
    @Override
    public double getSize() {
        double result;
        // 方钉计算成一个圆形，使它能够适合,并返回计算后的大小半径
        result = (Math.sqrt(Math.pow((peg.getSize() / 2), 2) * 2));
        return result;
    }
}
