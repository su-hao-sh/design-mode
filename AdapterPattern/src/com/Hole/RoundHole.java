package com.Hole;

import com.peg.RoundPeg;

/**
 * @Author 苏浩
 * @Date 2022/12/2 20:59
 * @Version 1.0
 * 这是一个圆形的洞，需要大小适合的钉子
 */
public class RoundHole {
    private double radius;

    public RoundHole(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    /**
     * 查看钉子与洞的大小是否合适
     * @param peg
     * @return
     */
    public boolean fits(RoundPeg peg) {
        boolean result;
        result = (this.getRadius() >= peg.getSize());
        return result;
    }

}
