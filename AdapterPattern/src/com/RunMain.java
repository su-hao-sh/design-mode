package com;

import com.AdapterSquare.AdapterSquare;
import com.Hole.RoundHole;
import com.peg.RoundPeg;
import com.peg.SquarePeg;

/**
 * @Author 苏浩
 * @Date 2022/12/2 21:10
 * @Version 1.0
 */
public class RunMain {
    public static void main(String[] args) {
        //我们创建一个半径5的洞
        RoundHole hole = new RoundHole(5);

        //测试1，创建一个半径为5的圆钉
        RoundPeg peg = new RoundPeg(5);
        Boolean b = hole.fits(peg);
        if(b) System.out.println("图钉适合半径为5的圆洞");

        //测试2 创建一个半径为5的方钉,通过适配器实现。
        SquarePeg squarePeg =new SquarePeg(5);
//        if( hole.fits(squarePeg)) System.out.println("");
        AdapterSquare adapterSquare = new AdapterSquare(squarePeg);
        if( hole.fits(adapterSquare)) System.out.println("方图钉适合半径为5的洞");


    }
}
