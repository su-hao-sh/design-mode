package com.example;

import com.example.Behavior.FlyBehavior;

/**
 * @Author 苏浩
 * @Date 2022/12/4 14:35
 * @Version 1.0
 */
public class yellowDuck extends Duck {
    public yellowDuck(FlyBehavior flyBehavior) {
        super(flyBehavior);
    }

    @Override
    public void iscolor() {
        System.out.println("黄色鸭子");
    }
}
