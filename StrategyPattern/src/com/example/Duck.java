package com.example;

import com.example.Behavior.FlyBehavior;

/**
 * @Author 苏浩
 * @Date 2022/12/4 14:23
 * @Version 1.0
 */
public abstract class Duck {
    protected FlyBehavior flyBehavior;

    public Duck(FlyBehavior flyBehavior){
        this.flyBehavior = flyBehavior;
    }
    public void quack(){//叫声
        System.out.println("嘎嘎");
    }
    public void swim(){//游泳
        System.out.println("擅长游泳");
    }

    public void fly(){
        flyBehavior.fly();
    }
    public abstract void iscolor();//是什么颜色
}
