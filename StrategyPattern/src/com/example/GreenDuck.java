package com.example;

import com.example.Behavior.FlyBehavior;

/**
 * @Author 苏浩
 * @Date 2022/12/4 14:24
 * @Version 1.0
 */
public class GreenDuck extends Duck {
    public GreenDuck(FlyBehavior flyBehavior) {
        super(flyBehavior);
    }

    public void iscolor() {
        System.out.println("是绿色");
    }
}
