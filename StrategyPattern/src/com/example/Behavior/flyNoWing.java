package com.example.Behavior;

/**
 * @Author 苏浩
 * @Date 2022/12/4 14:38
 * @Version 1.0
 */
public class flyNoWing implements FlyBehavior {
    @Override
    public void fly() {
        System.out.println("不会飞");
    }
}
