package com.example.Behavior;

/**
 * @Author 苏浩
 * @Date 2022/12/4 14:37
 * @Version 1.0
 */
public interface FlyBehavior {
    public void fly();
}
