package com.example;

import com.example.Behavior.flyNoWing;
import com.example.Behavior.flyWithWing;

/**
 * @Author 苏浩
 * @Date 2022/12/4 14:47
 * @Version 1.0
 */
public class RunMain {
    public static void main(String[] args) {
        Duck duck = new GreenDuck(new flyWithWing());
        duck.fly();
        duck.iscolor();
        duck.quack();
        duck.swim();
        System.out.println("----------------");

        Duck yellowDuck = new yellowDuck(new flyNoWing());
        yellowDuck.fly();
        yellowDuck.iscolor();
        yellowDuck.quack();
        yellowDuck.swim();
    }
}
