package com.example;

/**
 * @Author 苏浩
 * @Date 2022/12/4 22:33
 * @Version 1.0
 */
public class RunMain {
    public static void main(String[] args) {
        AbstractCook cook = new CookRice();
        cook.combination();
        System.out.println("-------------");
        AbstractCook cook2 = new CookTomato();
        cook2.combination();
    }
}
