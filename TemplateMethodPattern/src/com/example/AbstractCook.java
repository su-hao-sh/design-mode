package com.example;

/**
 * @Author 苏浩
 * @Date 2022/12/4 22:24
 * @Version 1.0
 */
public abstract class AbstractCook {
    public void openFire(){//共用方法
        System.out.println("生火");
    }

    public abstract void docook();//子类特有方法，做蛋炒饭，炒菜等，实现逻辑不同；

    public void closeFire(){//共用方法
        System.out.println("关火");
    }

    public void DoPlate(){//放入盘子中
        System.out.println("放入盘子中");
    }
    //.....

    public void combination(){
        this.openFire();
        this.docook();//子类特有方法
        this.closeFire();
        this.DoPlate();
    }
}
