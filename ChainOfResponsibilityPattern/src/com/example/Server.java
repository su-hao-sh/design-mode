package com.example;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author 苏浩
 * @Date 2022/12/4 16:06
 * @Version 1.0
 */
public class Server {
        private Map<String, String> users = new HashMap<>();
        private Middleware middleware;
        /**
         * Client passes a chain of object to server. This improves flexibility and
         * makes testing the server class easier.
         */
        public void setMiddleware(Middleware middleware) {
            this.middleware = middleware;
        }

        /**
         * Server gets email and password from client and sends the authorization
         * request to the chain.
         */
        public boolean logIn(String email, String password) {
            if (middleware.check(email, password)) {//走责任链，若全部走完返回true
                System.out.println("Authorization have been successful!");
                // Do something useful here for authorized users.
                return true;
            }
            return false;
        }

        public void register(String email, String password) {
            users.put(email, password);
        }

        public boolean hasEmail(String email) {//判断账号
            return users.containsKey(email);
        }

        public boolean isValidPassword(String email, String password) {//判断密码
            return users.get(email).equals(password);
        }
}
