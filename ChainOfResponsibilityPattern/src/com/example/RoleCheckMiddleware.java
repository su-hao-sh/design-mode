package com.example;

/**
 * @Author 苏浩
 * @Date 2022/12/4 16:06
 * @Version 1.0
 */
//检查用户角色
public class RoleCheckMiddleware  extends Middleware{
    public boolean check(String email, String password) {
        if (email.equals("admin@example.com")) {
            System.out.println("Hello, admin!");
            return true;
        }
        System.out.println("Hello, user!");
        return checkNext(email, password);
    }
}
