package com.example;

/**
 * @Author 苏浩
 * @Date 2022/12/4 16:04
 * @Version 1.0
 * 基础验证接口
 */
public abstract class Middleware {
    private Middleware next;

    /**
     * 构建责任链的类，通过链表头插法返回第一个类。
     */
    public static Middleware link(Middleware first, Middleware... chain) {
        Middleware head = first;
        for (Middleware nextInChain: chain) {
            head.next = nextInChain;
            head = nextInChain;
        }
        return first;
    }

    /**
     * 每个责任类实现该方法，写入具体逻辑，如果正确调用checkNext() 查看是否还有处理类。
     */
    public abstract boolean check(String email, String password);

    /**
     * 每一个check（）Method执行结束后，调用checkNext方法。
     */
    protected boolean checkNext(String email, String password) {
        if (next == null) {//到最后一个责任类后，返回为true。中途不满足条件责任类会返回false
            return true;
        }
        return next.check(email, password);
    }
}
