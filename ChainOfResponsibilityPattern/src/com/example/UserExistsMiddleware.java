package com.example;

/**
 * @Author 苏浩
 * @Date 2022/12/4 16:05
 * @Version 1.0
 * 检查用户登录信息
 */
public class UserExistsMiddleware extends Middleware {
    private Server server;

    public UserExistsMiddleware(Server server) {//注入server类，从server中获取Hashmap，来判断是否存在该账号
        this.server = server;
    }

    public boolean check(String email, String password) {
        if (!server.hasEmail(email)) {
            System.out.println("This email is not registered!");
            return false;
        }
        if (!server.isValidPassword(email, password)) {
            System.out.println("Wrong password!");
            return false;
        }
        return checkNext(email, password);
    }
}
