package com.example2;

/**
 * @Author 苏浩
 * @Date 2022/12/3 16:29
 * @Version 1.0
 */
public final class Singleton02 {
    private static volatile Singleton02 instance;//volatile :1.防止指令重排 2.提高可见性，线程修改变量后，其余线程及时更新

    public String value;

    private Singleton02(String value) {
        this.value = value;
    }

    public static Singleton02 getInstance(String value) {
        Singleton02 result = instance;
        if (result != null) {
            return result;
        }
        synchronized(Singleton02.class) {
            if (instance == null) {
                instance = new Singleton02(value);
            }
            return instance;
        }
    }
}
