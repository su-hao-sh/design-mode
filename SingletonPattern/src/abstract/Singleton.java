package com.example;

/**
 * @Author 苏浩
 * @Date 2022/12/3 16:27
 * @Version 1.0
 * 单线程适用，线程不安全
 */
public final class Singleton {
    private static Singleton instance;
    public String value;
    private Singleton(String value) {
        this.value = value;
    }

    public static Singleton getInstance(String value) {
        if (instance == null) {
            instance = new Singleton(value);
        }
        return instance;
    }
}
class Run{
    public static void main(String[] args) {
        Singleton singleton = Singleton.getInstance("FOO");
        Singleton anotherSingleton = Singleton.getInstance("BAR");
        System.out.println(singleton.value);
        System.out.println(anotherSingleton.value);
    }
}
