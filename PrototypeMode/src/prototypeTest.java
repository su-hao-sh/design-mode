/**
 * 原型模式
 */
public class prototypeTest {
    public static void main(String[] args) throws CloneNotSupportedException {
        Product product = new Product("wo", "ai", "ni", "me");
        System.out.println(product.toString());

        //这里用的是浅拷贝，指向的是同一块内存地址
        Product clone = product.clone();
        // 输出可以发现，两个对象的hashcode是不同的，说明两个对象不相同
        System.out.println(clone.toString());
    }
}

class Product  implements  Cloneable{
    private String param1;
    private String param2;
    private String param3;
    private String param4;

    public Product(String param1, String param2, String param3, String param4) {
        this.param1 = param1;
        this.param2 = param2;
        this.param3 = param3;
        this.param4 = param4;
    }

    public String getParam1() {
        return param1;
    }

    public void setParam1(String param1) {
        this.param1 = param1;
    }

    public String getParam2() {
        return param2;
    }

    public void setParam2(String param2) {
        this.param2 = param2;
    }

    public String getParam3() {
        return param3;
    }

    public void setParam3(String param3) {
        this.param3 = param3;
    }

    public String getParam4() {
        return param4;
    }

    public void setParam4(String param4) {
        this.param4 = param4;
    }

    @Override
    protected Product clone() throws CloneNotSupportedException {
        return ((Product) super.clone());
    }

    @Override
    public String toString() {
        //这里返回了对象的hashcode，用来判断是否是同一个对象
        return "Product{" +
                "hashcode = " + super.hashCode() + '\'' +
                "param1='" + param1 + '\'' +
                ", param2='" + param2 + '\'' +
                ", param3='" + param3 + '\'' +
                ", param4='" + param4 + '\'' +
                '}';
    }
}
