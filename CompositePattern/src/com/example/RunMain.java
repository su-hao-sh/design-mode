package com.example;

/**
 * @Author 苏浩
 * @Date 2022/12/3 22:03
 * @Version 1.0
 */
public class RunMain {
    public static void main(String[] args) {
        Menu menu = new Menu("主菜单");

        Menu menu1 = new Menu("子菜单1");
        Menu menu2 = new Menu("子菜单2");
        Menu menu3 = new Menu("子菜单3");

        //给所有菜单添加三个子菜单
        menu.add(menu1);
        menu.add(menu2);
        menu.add(menu3);

        //给第二个菜单添加一个菜单项
        menu2.add(new MenuItem("    子菜单2--炒饭", 10.0));
        MenuComponent menu2_2 = new MenuItem("    子菜单2--炒面",11.0);
        menu2.add(menu2_2);
        //给第3个菜单添加子菜单
        menu3.add(new Menu("  子菜单3---盖饭类"));
        menu3.add(new Menu("  子菜单3---粥类"));

        menu3.add(new MenuItem("  子菜单3---粥类--小米粥",4.0));
        //打印所有菜单
        menu.print();
    }
}
