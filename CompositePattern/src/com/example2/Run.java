package com.example2;

import java.awt.*;

/**
 * @Author 苏浩
 * @Date 2022/12/3 22:21
 * @Version 1.0
 */
public class Run {
    public static void main(String[] args) {
        ImageEditor editor = new ImageEditor();

        editor.loadShapes(
                new Circle(10, 10, 10, Color.BLUE),

                new CompoundShape(
                        new Circle(110, 110, 50, Color.RED),
                        new Dot(160, 160, Color.RED)
                ),

                new CompoundShape(
                        new Circle(250, 250, 100, Color.GREEN),
                        new Dot(240, 240, Color.GREEN),
                        new Circle(240, 360,100, Color.GREEN),
                        new Dot(360, 360, Color.GREEN),
                        new Dot(360, 240, Color.GREEN)
                )
        );
    }
}
