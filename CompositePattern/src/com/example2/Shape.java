package com.example2;

import java.awt.*;

/**
 * @Author 苏浩
 * @Date 2022/12/3 22:15
 * @Version 1.0
 */
public interface Shape {
    int getX();
    int getY();
    int getWidth();
    int getHeight();
    void move(int x, int y);
    boolean isInsideBounds(int x, int y);
    void select();
    void unSelect();
    boolean isSelected();
    void paint(Graphics graphics);
}
