package com.example;

/**
 * @Author 苏浩
 * @Date 2022/12/4 14:08
 * @Version 1.0
 */
public interface IFlyweight {
    void operation(String extrinsicState);
}
