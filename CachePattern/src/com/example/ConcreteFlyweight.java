package com.example;

/**
 * @Author 苏浩
 * @Date 2022/12/4 14:09
 * @Version 1.0
 */
//具体享元角色（ConcreteFlyweight）：实现抽象角色定义的业务。
// 该角色的内部状态处理应该与环境无关，不能出现会有一个操作改变内部状态 ，同时修改了外部状态
public class ConcreteFlyweight implements IFlyweight {
    private String intrinsicState;
    public ConcreteFlyweight(String intrinsicState) {
        this.intrinsicState = intrinsicState;
    }
    @Override
    public void operation(String extrinsicState) {
            System.out.println("Object address: " + System.identityHashCode(this));
            System.out.println("IntrinsicState: " + this.intrinsicState);
            System.out.println("ExtrinsicState: " + extrinsicState);

    }
}
