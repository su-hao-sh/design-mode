/**
 * 门面模式  外观模式
 */
public class FadedTest {
    public static void main(String[] args) {

    }
}

class client1{
    Faded faded = new Faded();
    public void doSomething(){
        faded.doSomething();
    }
}

class client2{
    Faded faded = new Faded();
    public void doSomething(){
        faded.doSomething();
    }
}

class Faded{

    public Faded() {}
    SubSystem subSystem = new SubSystem();
    SubSystem2 subSystem2 = new SubSystem2();
    SubSystem3 subSystem3 = new SubSystem3();

    public void doSomething(){
        subSystem.method1();
        subSystem2.method2();
        subSystem3.method3();
    }
}

class SubSystem{
    public void method1(){
        System.out.println("这是第一个方法的输出");
    }
}

class SubSystem2{
    public void method2(){
        System.out.println("这是第二个方法的输出");
    }
}

class SubSystem3{
    public void method3(){
        System.out.println("这是第三个方法的输出");
    }
}