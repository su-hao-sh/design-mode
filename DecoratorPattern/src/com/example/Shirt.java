package com.example;

/**
 * @Author 苏浩
 * @Date 2022/12/4 13:03
 * @Version 1.0
 */
public class Shirt extends ClothesDecorator {
    public Shirt(Person person) {
        super(person);
    }

    @Override
    public Double cost() {
        System.out.println(this.person.getClass().getName());
        return this.person.cost()+100.0;
    }

    @Override
    public void show() {
        person.show();
        System.out.println("买了一件衬衫 100.0");
    }
}
