package com.example;

/**
 * @Author 苏浩
 * @Date 2022/12/4 12:59
 * @Version 1.0
 */
public interface Person {
    public Double cost();
    public void show();

}
