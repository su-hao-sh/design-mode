import com.example.Person;
import com.example.Shirt;
import com.example.Shoes;
import com.example.xiaoMing;

/**
 * @Author 苏浩
 * @Date 2022/12/4 13:07
 * @Version 1.0
 */
public class RunMain  {
    public static void main(String[] args) {
        Person xiaoming = new xiaoMing();//创建小明对象
        xiaoming = new Shoes(xiaoming);//小明买了一件鞋，并穿上
        xiaoming = new Shirt(xiaoming);//小明买了一件衬衫，并穿上
        xiaoming.show();//会将小明买衣服的流程全部显示
        System.out.println("共花费："+xiaoming.cost());
    }
}
