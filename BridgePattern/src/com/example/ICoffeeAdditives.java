package com.example;

/**
 * @Author 苏浩
 * @Date 2022/12/3 21:10
 * @Version 1.0
 */
public interface ICoffeeAdditives {//将接口加入Coffee类的全局变量中，存储起来
    void addSomething();
}
//加奶
class Milk implements ICoffeeAdditives {
    @Override
    public void addSomething() {
        System.out.println("加奶");
    }
}
//加糖
class Sugar implements ICoffeeAdditives {
    @Override
    public void addSomething() {
        System.out.println("加糖");
    }
}
