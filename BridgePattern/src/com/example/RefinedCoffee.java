package com.example;

import java.util.Random;

/**
 * @Author 苏浩
 * @Date 2022/12/3 21:11
 * @Version 1.0
 */
public abstract class RefinedCoffee extends Coffee  {//扩展抽象化角色，改变和修正父类对抽象化的定义。
    public RefinedCoffee(ICoffeeAdditives additives) {
        super(additives);
    }
    public void checkQuality(){
        Random ran=new Random();
        System.out.println(String.format("%s 添加%s",additives.getClass().getSimpleName(),ran.nextBoolean()?"太多":"正常"));
    }
}

class LargeCoffee extends RefinedCoffee {

    public LargeCoffee(ICoffeeAdditives additives) {//将添加物传入
        super(additives);
    }
    @Override
    public void orderCoffee(int count) {
        additives.addSomething();//根据父类全局变量，调用Milk or Suger 的方法=》（通过多态）
        System.out.println("大杯咖啡" + count + "杯");
    }
}

class CoffeeWithSugar extends RefinedCoffee {

    public CoffeeWithSugar(ICoffeeAdditives additives) {
        super(additives);
    }
    @Override
    public void orderCoffee(int count) {
        additives.addSomething();
        System.out.println("中杯咖啡" + count + "杯");
    }
}

class MiniCoffeeWithSugar extends RefinedCoffee {

    public MiniCoffeeWithSugar(ICoffeeAdditives additives) {
        super(additives);
    }
    @Override
    public void orderCoffee(int count) {
        additives.addSomething();
        System.out.println("小杯咖啡" + count + "杯");
    }
}
