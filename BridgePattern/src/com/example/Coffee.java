package com.example;

/**
 * @Author 苏浩
 * @Date 2022/12/3 21:09
 * @Version 1.0
 */

/**
 * Coffee持有了ICoffeeAdditives 引用，
 * ICoffeeAdditives 的实例是通过构造函数注入的，这个过程就是我们所说的桥接过程
 *
 * 桥接模式：将m*n个实现类转换为m+n个实现类
 *
 * 题目： 容量上说有大杯，中杯，小杯。口味上说有原味，加糖。共需要创建3*2个类。
 *
 */
public abstract class Coffee {//核心
    protected ICoffeeAdditives additives;//将接口添加物，存储起来，将添加物加入到大杯中，通过构造方法super()==> 为additives赋值
    public Coffee(ICoffeeAdditives additives){
        this.additives=additives;
    }
    public abstract void orderCoffee(int count);
}
