package com.example;

/**
 * @Author 苏浩
 * @Date 2022/12/3 21:11
 * @Version 1.0
 */
public class RunMain {
    public static void main(String[] args) {
        //点两杯加奶的大杯咖啡
        RefinedCoffee largeWithMilk=new LargeCoffee(new Milk());
        largeWithMilk.orderCoffee(2);
        largeWithMilk.checkQuality();

        //点两杯加奶的中杯咖啡
        RefinedCoffee coffeeWithMilk=new CoffeeWithSugar(new Sugar());
        coffeeWithMilk.orderCoffee(6);
        coffeeWithMilk.checkQuality();
    }
}
