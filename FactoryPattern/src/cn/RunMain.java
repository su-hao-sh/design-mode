package cn;

import cn.abstractFactory.AbstractFactory;
import cn.abstractFactory.FactoryA;
import cn.abstractFactory.FactoryB;

/**
 * @Author 苏浩
 * @Date 2022/12/3 17:12
 * @Version 1.0
 */
public class RunMain {
    public static void main(String[] args) {
        //消费A牌的所有产品
        System.out.println("消费A牌的所有产品");
        AbstractFactory factory = new FactoryA();
        factory.createA().MyNameMethod();
        factory.createB().MyTypeMethod();

        //消费B牌的所有产品
        System.out.println("消费B牌的所有产品");
        AbstractFactory factoryB = new FactoryB();
        factoryB.createA().MyNameMethod();
        factoryB.createB().MyTypeMethod();

        //混搭A1 和 B2
        System.out.println("A混搭A1 和 B2产品");
        factory.createA().MyNameMethod();
        factoryB.createB().MyTypeMethod();
    }
}
