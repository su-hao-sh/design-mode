package cn.abstractFactory;

import cn.product.*;

/**
 * @Author 苏浩
 * @Date 2022/12/3 17:01
 * @Version 1.0
 */
public class FactoryB implements AbstractFactory {

    @Override
    public ProductA createA() {
        return new A2();
    }

    @Override
    public ProductB createB() {
        return new B2();
    }
}
