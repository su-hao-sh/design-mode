package cn.abstractFactory;

import cn.product.ProductA;
import cn.product.ProductB;

/**
 * @Author 苏浩
 * @Date 2022/12/3 17:00
 * @Version 1.0
 */
public interface AbstractFactory {
    public ProductA createA();
    public ProductB createB();
}
