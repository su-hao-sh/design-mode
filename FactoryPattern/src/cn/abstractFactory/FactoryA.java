package cn.abstractFactory;

import cn.product.A1;
import cn.product.B1;
import cn.product.ProductA;
import cn.product.ProductB;

/**
 * @Author 苏浩
 * @Date 2022/12/3 17:01
 * @Version 1.0
 */
public class FactoryA implements AbstractFactory {

    @Override
    public ProductA createA() {
        return new A1();
    }

    @Override
    public ProductB createB() {
        return new B1();
    }
}
