package com.Product;

import javax.annotation.Resource;

/**
 * @Author 苏浩
 * @Date 2022/11/29 15:16
 * @Version 1.0
 */
public class HTMLButton implements Button {
    @Override
    public void render() {
        System.out.println("<button>Test Button</button>");
        onClick();
    }

    @Override
    public void onClick() {
        System.out.println("Click! Button says - 'Hello World!'");
    }
}
