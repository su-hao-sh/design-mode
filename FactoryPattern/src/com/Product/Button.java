package com.Product;

/**
 * @Author 苏浩
 * @Date 2022/11/29 15:13
 * @Version 1.0
 */
public interface Button {
    void render();
    void onClick();
}
