package com.Product;

/**
 * @Author 苏浩
 * @Date 2022/11/29 15:48
 * @Version 1.0
 */
public class WindowsText implements Button {
    @Override
    public void render() {
        System.out.println("创建渲染windowsText按钮实例，并返回");
        onClick();
    }

    @Override
    public void onClick() {
        System.out.println("点击了 Windows 文本按钮");
    }
}
