package com.CreateFactory;

import com.Product.Button;
import com.Product.WindowsButton;
import com.Product.WindowsText;

/**
 * @Author 苏浩
 * @Date 2022/11/29 15:25
 * @Version 1.0
 */
public class WindowsTextFactory extends fatherFactor {

    @Override
    Button createButton() {
        return new WindowsText();
    }

    @Override
    public void render() {
        Button okButton = createButton();
        System.out.println("我WindowsTextFactory重写了工厂父类");
        okButton.onClick();
        okButton.render();
    }
}
