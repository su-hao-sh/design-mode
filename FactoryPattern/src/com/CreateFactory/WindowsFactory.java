package com.CreateFactory;

import com.Product.Button;
import com.Product.WindowsButton;

/**
 * @Author 苏浩
 * @Date 2022/11/29 15:25
 * @Version 1.0
 */
public class WindowsFactory extends fatherFactor {
    @Override
    Button createButton() {
        return new WindowsButton();
    }
}
