package com;

import com.CreateFactory.HtmlFactory;
import com.CreateFactory.WindowsFactory;
import com.CreateFactory.WindowsTextFactory;
import com.CreateFactory.fatherFactor;

/**
 * @Author 苏浩
 * @Date 2022/11/29 15:24
 * @Version 1.0
 */
public class RunMain {
    private static fatherFactor factor;
    public static void main(String[] args) {
        init();
        factor.render();
    }

    public static void init(){
        // 程序根据当前配置或环境设定选择创建者的类型。
             String  config = "Windows";
                if (config == "Windows")
                    factor = new WindowsFactory();
                else if (config == "Web")
                    factor = new HtmlFactory();
                else if (config=="Text")
                    factor = new WindowsTextFactory();
                else
                    throw new RuntimeException("错误！未知的操作系统。");

    }
}
